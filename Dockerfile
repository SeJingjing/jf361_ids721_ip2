# Use an official Rust image as the base
FROM rust:1.68 as builder

# Create a new empty shell project
RUN USER=root cargo new --bin indivisual_project2
WORKDIR /indivisual_project2

# Copy the Cargo manifest files and build only the dependencies
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
RUN cargo build --release
RUN rm src/*.rs

# Copy the source code and build the application
COPY ./src ./src
RUN rm ./target/release/deps/indivisual_project2*
RUN cargo build --release

# Use a minimal Debian image for the runtime
FROM debian:buster-slim
COPY --from=builder /indivisual_project2/target/release/indivisual_project2 /usr/local/bin/
EXPOSE 8080
CMD ["indivisual_project2"]
