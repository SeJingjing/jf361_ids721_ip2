# jf361_ids721_ip2

## Project Introduction
This project implements a Rust Microservice.

## Project Description
- Simple REST API/web service in Rust
- Dockerfile to containerize service
- CI/CD pipeline files

## Setup
Prerequisites:
Rust and Docker installed on your machine

Step 1: Create a new Cargo project in terminal and change into the new directory
```shell
cargo new <project-name>
cd <project-name>
```
Step 2: Add dependencies in `Cargo.toml` and implement the web service function in `src/main.rs`

Step 3: Create the `Dockerfile` in the root of the project

Step 4: Run below commands in terminal
```shell
cargo build
cargo run
```
Step 5: Make sure the `Docker Desktop` is open. Then run below commands in terminal
```shell
docker build -t <image-name> .
docker run -p 8080:8080 <image-name>
```
Then, you can now access the Actix web app at http://localhost:8080.

Step 6: CI/CD Pipeline
Create a `.gitlab-ci.yml` file to enable the CI/CD pipeline.

## Rust Microservice Functionality
http://localhost:8080
<p align="center">
  <img src="main.png" />
</p>

http://localhost:8080/greet?name=Jingjing&age=22&major=ECE
<p align="center">
  <img src="greet.png" />
</p>

Run `curl -X POST -H "Content-Type: application/json" -d '{"name":"Jingjing","age":22,"major":"ECE"}' http://localhost:8080/echo` in terminal.
<p align="center">
  <img src="echo.png" />
</p>

## Docker Configuration
`Dockerfile`: 
```angular2html
# Use an official Rust image as the base
FROM rust:1.68 as builder

# Create a new empty shell project
RUN USER=root cargo new --bin indivisual_project2
WORKDIR /indivisual_project2

# Copy the Cargo manifest files and build only the dependencies
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
RUN cargo build --release
RUN rm src/*.rs

# Copy the source code and build the application
COPY ./src ./src
RUN rm ./target/release/deps/indivisual_project2*
RUN cargo build --release

# Use a minimal Debian image for the runtime
FROM debian:buster-slim
COPY --from=builder /indivisual_project2/target/release/indivisual_project2 .
EXPOSE 8080
CMD ["indivisual_project2"]
```

Container screenshot
<p align="center">
  <img src="container.png" />
</p>

Image screenshot
<p align="center">
  <img src="image.png" />
</p>


## CI/CD Pipeline
Using a CI/CD pipeline in GitLab enables automatic building, testing, and validation of our Dockerized project directly within the GitLab pipeline. If it fails, the pipeline will provide an error message indicating the issue.
```
# Define stages for the pipeline
stages:
  - build

# Define variables that can be used across jobs
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

build:
  stage: build
  image: docker:stable  # Use the Docker image for running Docker commands
  services:
    - docker:dind  # Enable Docker-in-Docker service for building Docker images
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t individual_project2 .
    - docker run -d -p 8088:8088 individual_project2
    - docker ps -a

```


## Demo
The Demo video: [Demo Video.mov](Demo%20video.mov)

